/// Selected optimization methods.
extern crate linalg;

use crate::functions::{BlackMagic, Function};
use linalg::error::MathError;
use linalg::lup::lup_invert;
use linalg::matrix::{Matrix, RealMat};
use linalg::vector::{RealVec, Vector};

/// Precision coefficient for checking whether two doubles are the same.
const PRECISION: f64 = 1e-6;

type Result<T> = std::result::Result<T, MathError>;

/// Checks whether the corresponding components (corresponding to 0..n)
/// of x and y are within the difference bounds. In other words, the function
/// checks whether the corresponding elements in vectors x and y are the same,
/// regulated by the precision factor.
fn vectors_same(x: &RealVec, y: &RealVec, e: f64) -> bool {
    x.values()
        .iter()
        .zip(y.values().iter())
        .all(|(a, b)| (a - b).abs() < e)
}

fn add_in_dimension(vector: &mut RealVec, offset: f64, dim: usize) {
    let val = vector.get_elem(dim).unwrap();
    vector.set_elem(dim, val + offset);
}

fn sub_in_dimension(vector: &mut RealVec, offset: f64, dim: usize) {
    let val = vector.get_elem(dim).unwrap();
    vector.set_elem(dim, val - offset);
}

pub fn unimodal_interval<F: Function>(
    offset: f64,
    starting_point: &RealVec,
    dimension: usize,
    f: &mut F,
) -> (f64, f64) {
    let mut lower = starting_point.clone();
    sub_in_dimension(&mut lower, offset, dimension);

    let mut upper = starting_point.clone();
    add_in_dimension(&mut upper, offset, dimension);

    let mut step = 1.0;
    let mut m = starting_point.get_elem(dimension).unwrap();

    let mut l = starting_point.get_elem(dimension).unwrap() - offset;
    let mut r = starting_point.get_elem(dimension).unwrap() + offset;

    let mut fm = f.apply(starting_point);
    let mut fl = f.apply(&lower);
    let mut fr = f.apply(&upper);

    //println!("fm: {}, fl: {}, fr: {}", fm, fl, fr);
    if fm < fr && fm < fl {
        return (l, r);
    } else if fm > fr {
        loop {
            l = m;
            m = upper.get_elem(dimension).unwrap();
            fm = fr;

            upper = starting_point.clone();
            step *= 2.0;
            add_in_dimension(&mut upper, offset * step, dimension);
            r = starting_point.get_elem(dimension).unwrap() + offset * step;

            fr = f.apply(&upper);
            //println!("fm: {}, fl: {}, fr: {}", fm, fl, fr);
            if fm <= fr {
                break;
            }
        }
    } else {
        loop {
            r = m;
            m = lower.get_elem(dimension).unwrap();
            fm = fl;

            lower = starting_point.clone();
            step *= 2.0;
            sub_in_dimension(&mut lower, offset * step, dimension);
            l = starting_point.get_elem(dimension).unwrap() - offset * step;

            fl = f.apply(&lower);
            //println!("fm: {}, fl: {}, fr: {}", fm, fl, fr);
            if fl >= fm {
                break;
            }
        }
    }

    (l, r)
}

pub fn golden_cut<F: Function>(
    init: &RealVec,
    range: Option<(f64, f64)>,
    f: &mut F,
    e: Option<f64>,
    h: Option<f64>,
    dimension: usize,
) -> RealVec {
    let h = h.unwrap_or(1.0);
    let e = e.unwrap_or(PRECISION);
    let (x_lo, x_hi) = range.unwrap_or(unimodal_interval(h, init, dimension, f));

    //println!("Starting golden cut with interval [{}, {}]", x_lo, x_hi);
    golden_cut_opt(x_lo, x_hi, init, f, e, dimension)
}

fn golden_cut_opt<F: Function>(
    a: f64,
    b: f64,
    starting_point: &RealVec,
    f: &mut F,
    e: f64,
    dimension: usize,
) -> RealVec {
    let k = 0.5 * (5.0_f64.sqrt() - 1.0);
    let mut a = a;
    let mut b = b;
    let shift = k * (b - a);

    let mut c = starting_point.clone();
    c.set_elem(dimension, b);
    sub_in_dimension(&mut c, shift, dimension);

    let mut d = starting_point.clone();
    d.set_elem(dimension, a);
    add_in_dimension(&mut d, shift, dimension);

    let mut fc = f.apply(&c);
    let mut fd = f.apply(&d);

    while (b - a) > e {
        if fc < fd {
            b = d.get_elem(dimension).unwrap();
            d = c.clone();

            let shift = k * (b - a);
            c.set_elem(dimension, b);
            sub_in_dimension(&mut c, shift, dimension);

            fd = fc;
            fc = f.apply(&c);
        } else {
            a = c.get_elem(dimension).unwrap();
            c = d.clone();

            let shift = k * (b - a);
            d.set_elem(dimension, a);
            add_in_dimension(&mut d, shift, dimension);

            fc = fd;
            fd = f.apply(&d);
        }
    }
    //println!("Found best: {} {}", a, b);
    let new = (a + b) / 2.0;
    let mut res = starting_point.clone();
    res.set_elem(dimension, new);
    res
}

pub fn coordinate_search<F: Function>(init: &RealVec, f: &mut F, e: Option<f64>) -> RealVec {
    let e = e.unwrap_or(PRECISION);
    let n = init.get_dim();

    let mut x = init.clone();
    loop {
        let xs = x.clone();
        for i in 0..n {
            x = golden_cut(&x, None, f, Some(e), None, i);
        }
        if vectors_same(&x, &xs, e) {
            break;
        }
    }
    x
}

pub fn hooke_jeeves<F: Function>(
    init: &RealVec,
    f: &mut F,
    e: Option<f64>,
    dx: Option<f64>,
) -> RealVec {
    let e = e.unwrap_or(PRECISION);
    let mut dx = dx.unwrap_or(0.5);

    let mut base = init.clone();
    let mut starting = init.clone();

    loop {
        let new = hj_explore(&starting, dx, f);
        if f.apply(&new) < f.apply(&base) {
            starting = new.clone().scale(2.0).sub(&base).unwrap();
            base = new.clone();
        } else {
            dx = 0.5 * dx;
            starting = base.clone();
        }

        if dx < e {
            break;
        }
    }
    base
}

fn hj_explore<F: Function>(starting: &RealVec, dx: f64, f: &mut F) -> RealVec {
    let mut x = starting.clone();
    let n = x.get_dim();

    for i in 0..n {
        let p = f.apply(&x);
        add_in_dimension(&mut x, dx, i);
        let n = f.apply(&x);

        if n > p {
            sub_in_dimension(&mut x, 2.0 * dx, i);
            let n = f.apply(&x);
            if n > p {
                add_in_dimension(&mut x, dx, i);
            }
        }
    }
    x
}

pub fn simplex_default<F: Function>(f: &mut F, x_init: &RealVec) -> RealVec {
    simplex_opt(f, x_init, 1.0, 1.0, 0.5, 2.0, 0.5, PRECISION)
}

pub fn simplex_custom<F: Function>(
    f: &mut F,
    x_init: &RealVec,
    step: Option<f64>,
    alpha: Option<f64>,
    beta: Option<f64>,
    gamma: Option<f64>,
    sigma: Option<f64>,
    e: Option<f64>,
) -> RealVec {
    let step = step.unwrap_or(1.0);
    let alpha = alpha.unwrap_or(1.0);
    let beta = beta.unwrap_or(0.5);
    let gamma = gamma.unwrap_or(2.0);
    let sigma = sigma.unwrap_or(0.5);
    let e = e.unwrap_or(PRECISION);

    simplex_opt(f, x_init, step, alpha, beta, gamma, sigma, e)
}

fn simplex_opt<F: Function>(
    f: &mut F,
    x_init: &RealVec,
    step: f64,
    alpha: f64,
    beta: f64,
    gamma: f64,
    sigma: f64,
    e: f64,
) -> RealVec {
    let n = x_init.get_dim();
    let mut simplex = Vec::new();
    simplex.push(SimplexSolution::new(&x_init, f));
    let mut iter_ctr = 0;

    for i in 0..n {
        let mut v = x_init.clone();
        v.set_elem(i, v.get_elem(i).unwrap() + step).unwrap();
        simplex.push(SimplexSolution::new(&v, f));
    }

    loop {
        //println!("Simplex: {:?}", simplex);
        let (l, h) = simplex_calc_indices(&simplex);
        //println!("Found best and worst: {} {}", l, h);
        //println!("Best: {:?}   |   worst: {:?}", simplex[l], simplex[h]);
        let centroid = simplex_find_centroid(&simplex, h, f);
        //println!("Found centroid: {}", centroid.solution());
        let reflected = simplex_reflect(alpha, centroid.solution(), simplex[h].solution(), f);
        //println!("Reflected: {}", reflected.solution());

        if reflected.fitness() < simplex[l].fitness() {
            let expanded = simplex_expand(gamma, centroid.solution(), reflected.solution(), f);
            //println!("Expanded: {}", expanded.solution());
            if expanded.fitness() < simplex[l].fitness() {
                simplex[h] = expanded;
            } else {
                simplex[h] = reflected;
            }
        } else {
            if simplex_evaluate_all(&reflected, &simplex, h) {
                if reflected.fitness() < simplex[h].fitness() {
                    simplex[h] = reflected;
                }
                let contracted =
                    simplex_contract(beta, centroid.solution(), simplex[h].solution(), f);
                //println!("Contracted: {}", contracted.solution());
                if contracted.fitness() < simplex[h].fitness() {
                    simplex[h] = contracted;
                } else {
                    for i in 0..simplex.len() {
                        if i == l {
                            continue;
                        }
                        let mut new = simplex[l].solution().add(simplex[i].solution()).unwrap();
                        new.scale_inplace(sigma);
                        simplex[i] = SimplexSolution::new(&new, f);
                    }
                }
            } else {
                simplex[h] = reflected;
            }
        }

        if simplex_stop(&simplex, &centroid, e, iter_ctr) {
            let (b, _) = simplex_calc_indices(&simplex);
            //println!("{:?}", simplex);
            if step < 0.5 {
                return simplex[b].solution().clone();
            } else {
                return simplex_custom(
                    f,
                    &simplex[b].solution,
                    Some(step * 0.5),
                    Some(alpha),
                    Some(beta),
                    Some(gamma),
                    Some(sigma),
                    Some(e),
                );
            }
        }
        iter_ctr += 1;
    }
}

fn simplex_stop(
    simplex: &Vec<SimplexSolution>,
    centroid: &SimplexSolution,
    e: f64,
    iter_ctr: usize,
) -> bool {
    let iter_limit = 1000;
    let n = simplex.len();
    let mut sum = 0.0;
    for s in simplex.iter() {
        let temp = (s.fitness() - centroid.fitness()).powi(2);
        sum += temp;
    }
    sum = sum / (n as f64);
    sum.sqrt() < e || iter_ctr >= iter_limit
}

fn simplex_calc_indices(simplex: &Vec<SimplexSolution>) -> (usize, usize) {
    let mut lowest = 1e100;
    let mut l = 0;
    let mut highest = 0.0;
    let mut h = 0;
    simplex.iter().zip(0..).for_each(|(x, i)| {
        if x.fitness() < lowest {
            lowest = x.fitness();
            l = i;
        } else if x.fitness() > highest {
            highest = x.fitness();
            h = i;
        }
    });
    (l, h)
}

fn simplex_find_centroid<F: Function>(
    simplex: &Vec<SimplexSolution>,
    highest: usize,
    f: &mut F,
) -> SimplexSolution {
    let n = simplex[0].solution().get_dim();
    let mut centroid = RealVec::from_vec(vec![0.0; n]);

    for i in 0..simplex.len() {
        if i == highest {
            continue;
        }
        centroid.add_inplace(simplex[i].solution()).unwrap();
    }
    let length = simplex.len() - 1;
    centroid.scale_inplace(1.0 / length as f64);
    SimplexSolution::new(&centroid, f)
}

fn simplex_evaluate_all(
    reflected: &SimplexSolution,
    simplex: &Vec<SimplexSolution>,
    h: usize,
) -> bool {
    for i in 0..simplex.len() {
        if i == h {
            continue;
        }
        if reflected.fitness() <= simplex[i].fitness() {
            return false;
        }
    }
    true
}

fn simplex_reflect<F: Function>(
    alpha: f64,
    centroid: &RealVec,
    worst: &RealVec,
    f: &mut F,
) -> SimplexSolution {
    let reflected = centroid
        .sub(worst)
        .unwrap()
        .scale(alpha)
        .add(centroid)
        .unwrap();

    SimplexSolution::new(&reflected, f)
}

fn simplex_expand<F: Function>(
    gamma: f64,
    centroid: &RealVec,
    reflected: &RealVec,
    f: &mut F,
) -> SimplexSolution {
    let expanded = reflected
        .sub(centroid)
        .unwrap()
        .scale(gamma)
        .add(centroid)
        .unwrap();

    SimplexSolution::new(&expanded, f)
}

fn simplex_contract<F: Function>(
    beta: f64,
    centroid: &RealVec,
    worst: &RealVec,
    f: &mut F,
) -> SimplexSolution {
    //println!("Contracting centroid: {} and worst: {}", centroid, worst);
    let contracted = centroid.scale(1.0 - beta).add(&worst.scale(beta)).unwrap();
    SimplexSolution::new(&contracted, f)
}

#[derive(Debug)]
pub struct SimplexSolution {
    solution: RealVec,

    value: f64,
}

impl SimplexSolution {
    pub fn new<F: Function>(x: &RealVec, f: &mut F) -> SimplexSolution {
        SimplexSolution {
            solution: x.clone(),
            value: f.apply(x),
        }
    }

    pub fn from_solution(other: &SimplexSolution) -> SimplexSolution {
        SimplexSolution {
            solution: other.solution().clone(),
            value: other.fitness(),
        }
    }

    pub fn solution(&self) -> &RealVec {
        &self.solution
    }

    pub fn fitness(&self) -> f64 {
        self.value
    }

    pub fn update<F: Function>(&mut self, f: &mut F) {
        self.value = f.apply(&self.solution);
    }
}

pub fn gradient_descent<T: Function>(
    f: &mut T,
    x_init: &RealVec,
    step: f64,
    use_golden_cut: bool,
    e: f64,
) -> Result<RealVec> {
    let mut x_new = x_init.clone();
    let mut x_curr;
    let mut gradient;

    loop {
        x_curr = x_new.clone();
        gradient = f.gradient(&x_curr);

        if use_golden_cut {
            let mut helper = BlackMagic {
                x: x_curr.clone(),
                grad: gradient.clone(),
                f: f,
            };
            let zero = RealVec::from_vec(vec![0.0]);
            let l = golden_cut(&zero, None, &mut helper, Some(e / 10.0), None, 0)
                .get_elem(0)
                .unwrap();
            x_new = x_curr.sub(&gradient.scale(l))?;
        } else {
            x_new = x_curr.sub(&gradient.scale(step))?;
        }

        if gradient.norm() < e {
            return Ok(x_new);
        }
    }
}

pub fn newton_raphson<T: Function>(
    f: &mut T,
    x_init: &RealVec,
    use_golden_cut: bool,
    e: f64,
) -> Result<RealVec> {
    let mut x_new = x_init.clone();
    let mut x_curr;
    let mut hess;
    let mut grad;

    loop {
        x_curr = x_new.clone();
        grad = f.gradient(&x_curr);
        hess = f.hessian(&x_curr);

        let dx = lup_invert(&hess)?.mul(&RealMat::from_vec_into_col(&grad)?)?;
        let dx = RealVec::from_col_matrix(&dx)?;

        if use_golden_cut {
            let xf = x_curr.clone();
            let dxf = dx.clone();
            let mut helper = BlackMagic {
                f: f,
                x: xf,
                grad: dxf,
            };

            let zero = RealVec::from_vec(vec![0.0]);
            let l = golden_cut(&zero, None, &mut helper, Some(e / 10.0), None, 0)
                .get_elem(0)
                .unwrap();
            x_new = x_curr.sub(&dx.scale(l))?;
        } else {
            x_new = x_curr.sub(&dx)?;
        }

        if x_new.sub(&x_curr)?.norm() < e {
            return Ok(x_new);
        }
    }
}

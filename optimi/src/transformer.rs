extern crate linalg;

use crate::functions::Function;
use crate::methods::hooke_jeeves;
use linalg::vector::{RealVec, Vector};
use linalg::matrix::RealMat;

pub struct MixedTransform<'a, T: Function> {
    f: &'a mut T,

    gs: Vec<&'a mut T>,

    hs: Vec<&'a mut T>,

    t: f64,

}

impl<'a, T: Function> Function for MixedTransform<'a, T> {

    fn apply(&mut self, x: &RealVec) -> f64 {
        let mut gsum = 0.0;
        for g in self.gs.iter_mut() {
            let mut v = g.apply(x);
            if v <= 0.0 {
                v = 1e-9;
            }
            gsum += v.ln();
        }

        let mut hsum = 0.0;
        for h in self.hs.iter_mut() {
            let v = h.apply(x).powi(2);
            hsum += v;
        }
        self.f.apply(x) - (1.0 / self.t) * gsum + self.t * hsum
    }

    fn gradient(&mut self, _: &RealVec) -> RealVec {
        panic!("Kaj ce ti to lmao");
    }

    fn hessian(&mut self, _: &RealVec) -> RealMat {
        panic!("Kaj ce ti to lmao");
    }
}

struct ConstraintHelper<'a, 'b, T: 'b + Function> {
    transformer: &'a mut MixedTransform<'b, T>,
}

impl<'a, 'b, T: 'b + Function> Function for ConstraintHelper<'a, 'b, T>{
    fn apply(&mut self, x: &RealVec) -> f64 {
        let mut gsum = 0.0;
        for g in self.transformer.gs.iter_mut() {
            let result = g.apply(x);
            let t = if result >= 0.0 {
                0.0
            } else {
                1.0
            };
            gsum += t * result;
        }
        -gsum
    }

    fn gradient(&mut self, _: &RealVec) -> RealVec {
        panic!("Lmao ne zovi me hahaha");
    }

    fn hessian(&mut self, _: &RealVec) -> RealMat {
        panic!("Sta ce ti to");
    }
}

impl<'a, T: Function> MixedTransform<'a, T> {
    pub fn new(f: &'a mut T, gs: Vec<&'a mut T>, hs: Vec<&'a mut T>, t: Option<f64>) -> MixedTransform<'a, T> {
        MixedTransform { f: f, gs: gs, hs: hs, t: t.unwrap_or(1.0) }
    }

    pub fn update_t(&mut self, new_t: f64) {
        self.t = new_t;
    }

    pub fn get_t(&mut self) -> f64 {
        return self.t.clone()
    }
}

pub fn mixed_transform<T: Function>(transformer: &mut MixedTransform<T>, x_init: &RealVec, t_init: f64, e: f64) -> RealVec {
    let mut x = x_init.clone();
    transformer.update_t(t_init);

    if !is_point_legal(transformer, &x) {
        let mut helper = ConstraintHelper { transformer: transformer };
        x = hooke_jeeves(&x, &mut helper, None, None);
    }

    loop {
        let x_new = hooke_jeeves(&x, transformer, None, None);

        if x.sub(&x_new).unwrap().norm() < e {
            return x;
        }
        let old_t = transformer.get_t();
        x = x_new;
        transformer.update_t(old_t * 10.0);
    }
}

fn is_point_legal<T: Function>(transformer: &mut MixedTransform<T>, x: &RealVec) -> bool {
    for g in transformer.gs.iter_mut() {
        if g.apply(x) < 0.0 {
            return false;
        }
    }

    for h in transformer.hs.iter_mut() {
        if h.apply(x).abs() > 1e-6 {
            return false;
        }
    }
    
    true
}
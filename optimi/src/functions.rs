/// Target scalar functions.
extern crate linalg;

use linalg::matrix::*;
use linalg::vector::*;

pub trait Function {
    fn apply(&mut self, x: &RealVec) -> f64;
    fn gradient(&mut self, x: &RealVec) -> RealVec;
    fn hessian(&mut self, x: &RealVec) -> RealMat;
}

type ScalarFun = fn(&RealVec) -> f64;
type GradientFun = fn(&RealVec) -> RealVec;
type HessianFun = fn(&RealVec) -> RealMat;

#[derive(Clone)]
pub struct FnCaller {
    ctr: usize,

    grad_ctr: usize,

    hess_ctr: usize,

    f: ScalarFun,

    grad_fn: Option<GradientFun>,

    hess_fn: Option<HessianFun>,
}

impl FnCaller {
    pub fn from_func(f: ScalarFun) -> FnCaller {
        FnCaller {
            ctr: 0,
            grad_ctr: 0,
            hess_ctr: 0,
            f: f,
            grad_fn: None,
            hess_fn: None,
        }
    }

    pub fn with_gradients(f: ScalarFun, g: GradientFun, h: HessianFun) -> FnCaller {
        FnCaller {
            ctr: 0,
            grad_ctr: 0,
            hess_ctr: 0,
            f: f,
            grad_fn: Some(g),
            hess_fn: Some(h),
        }
    }

    pub fn reset_count(&mut self) {
        self.ctr = 0;
        self.grad_ctr = 0;
        self.hess_ctr = 0;
    }

    pub fn call_count(&self) -> usize {
        self.ctr + self.grad_ctr + self.hess_ctr
    }

    pub fn func_count(&self) -> usize {
        self.ctr
    }

    pub fn grad_count(&self) -> usize {
        self.grad_ctr
    }

    pub fn hess_count(&self) -> usize {
        self.hess_ctr
    }

    pub fn print_calls(&self) {
        println!(
            "Fn calls: {}  |  Grad calls: {}  |  Hessian calls: {}  |  Total: {}",
            self.ctr,
            self.grad_ctr,
            self.hess_ctr,
            self.ctr + self.grad_ctr + self.hess_ctr
        );
    }
}

pub struct BlackMagic<'a, T: Function> {
    pub x: RealVec,

    pub grad: RealVec,

    pub f: &'a mut T,
}

impl<'a, T: Function> Function for BlackMagic<'a, T> {
    fn apply(&mut self, x: &RealVec) -> f64 {
        let l = x.get_elem(0).unwrap();
        let grd = self.grad.scale(l);
        let new_pt = self.x.sub(&grd).unwrap();
        self.f.apply(&new_pt)
    }

    fn gradient(&mut self, _: &RealVec) -> RealVec {
        // Ovo ne radi sto mislis da radi
        panic!("Ovo ti ne treba lmao");
    }

    fn hessian(&mut self, _: &RealVec) -> RealMat {
        // Ovo ne radi sto mislis da radi
        // Ovo je prilicno beskorisno
        panic!("Ovo ti ne treba lmao");
    }
}

impl Function for FnCaller {
    fn apply(&mut self, x: &RealVec) -> f64 {
        self.ctr += 1;
        (self.f)(x)
    }

    fn gradient(&mut self, x: &RealVec) -> RealVec {
        self.grad_ctr += 1;
        let grad = self.grad_fn.as_ref().unwrap();
        (grad)(x)
    }

    fn hessian(&mut self, x: &RealVec) -> RealMat {
        self.hess_ctr += 1;
        let hess = self.hess_fn.as_ref().unwrap();
        (hess)(x)
    }
}

/// Rosenbrock's 'banana' function.
pub fn f_1(x: &RealVec) -> f64 {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();
    100.0 * (x2 - x1.powi(2)).powi(2) + (1.0 - x1).powi(2)
}

pub fn f_2(x: &RealVec) -> f64 {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();
    (x1 - 4.0).powi(2) + 4.0 * (x2 - 2.0).powi(2)
}

pub fn f_3(x: &RealVec) -> f64 {
    let n = x.get_dim();
    x.values()
        .iter()
        .zip(1..=n)
        .fold(0.0, |sum, (x_i, i)| sum + (x_i - i as f64).powi(2))
}

pub fn f_3a(x: &RealVec) -> f64 {
    let x = x.get_elem(0).unwrap();
    (x - 3.0).powi(2)
}

/// Jakobovic's function :)
pub fn f_4(x: &RealVec) -> f64 {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();
    ((x1 - x2) * (x1 + x2)).abs() + (x1.powi(2) + x2.powi(2)).sqrt()
}

/// Schaffer's function
pub fn f_5(x: &RealVec) -> f64 {
    let sum = x.values().iter().fold(0.0, |sum, x_i| sum + x_i.powi(2)); //Sum of squares
    0.5 + (sum.sqrt().sin().powi(2) - 0.5) / (1.0 + 0.001 * sum).powi(2)
}

pub fn interval_square_poly(x: &RealVec) -> f64 {
    (x.get_elem(0).unwrap() - 2.0).powi(2)
}

pub fn golden_cut_square_poly(x: &RealVec) -> f64 {
    (x.get_elem(0).unwrap() - 4.0).powi(2)
}

pub fn lab3_fn1(x: &RealVec) -> f64 {
    f_1(x)
}

pub fn lab3_der1(x: &RealVec) -> RealVec {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();
    let d1 = 2.0 * (200.0 * x1.powi(3) - 200.0 * x1 * x2 + x1 - 1.0);
    let d2 = 200.0 * (x2 - x1 * x1);

    RealVec::from_vec(vec![d1, d2])
}

pub fn lab3_hes1(x: &RealVec) -> RealMat {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();

    let d11 = -400.0 * (x2 - x1.powi(2))+ 800.0 * x1.powi(2) + 2.0;
    let d12 = -400.0 * x1;
    let d22 = 200.0;

    let mut res = RealMat::from_dims(2, 2);
    res.set_elem(0, 0, d11).unwrap();
    res.set_elem(0, 1, d12).unwrap();
    res.set_elem(1, 0, d12).unwrap();
    res.set_elem(1, 1, d22).unwrap();
    res
}

pub fn lab3_fn2(x: &RealVec) -> f64 {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();

    (x1 - 4.0).powi(2) + 4.0 * (x2 - 2.0).powi(2)
}

pub fn lab3_der2(x: &RealVec) -> RealVec {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();
    let d1 = 2.0 * (x1 - 4.0);
    let d2 = 8.0 * (x2 - 2.0);

    RealVec::from_vec(vec![d1, d2])
}

pub fn lab3_hes2(_: &RealVec) -> RealMat {
    let mut res = RealMat::from_dims(2, 2);
    res.set_elem(0, 0, 2.0).unwrap();
    res.set_elem(0, 1, 0.0).unwrap();
    res.set_elem(1, 0, 0.0).unwrap();
    res.set_elem(1, 1, 8.0).unwrap();

    res
}

pub fn lab3_fn3(x: &RealVec) -> f64 {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();

    (x1 - 2.0).powi(2) + (x2 + 3.0).powi(2)
}

pub fn lab3_der3(x: &RealVec) -> RealVec {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();
    let d1 = 2.0 * (x1 - 2.0);
    let d2 = 2.0 * (x2 + 3.0);

    RealVec::from_vec(vec![d1, d2])
}

pub fn lab3_hes3(_: &RealVec) -> RealMat {
    let mut res = RealMat::from_dims(2, 2);
    res.set_elem(0, 0, 2.0).unwrap();
    res.set_elem(0, 1, 0.0).unwrap();
    res.set_elem(1, 0, 0.0).unwrap();
    res.set_elem(1, 1, 2.0).unwrap();

    res
}

pub fn lab3_fn4(x: &RealVec) -> f64 {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();

    (x1 - 3.0).powi(2) + x2 * x2
}

pub fn lab3_der4(x: &RealVec) -> RealVec {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();
    let d1 = 2.0 * (x1 - 3.0);
    let d2 = 2.0 * x2;

    RealVec::from_vec(vec![d1, d2])
}

pub fn lab3_hes4(_: &RealVec) -> RealMat {
    let mut res = RealMat::from_dims(2, 2);
    res.set_elem(0, 0, 2.0).unwrap();
    res.set_elem(0, 1, 0.0).unwrap();
    res.set_elem(1, 0, 0.0).unwrap();
    res.set_elem(1, 1, 2.0).unwrap();

    res
}

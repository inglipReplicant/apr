extern crate linalg;

use crate::functions::*;
use crate::methods::*;
use linalg::vector::RealVec;

type ScalarFun = fn(&RealVec) -> f64;

pub fn run_simplex(init: &RealVec, f: ScalarFun) {
    let mut caller = FnCaller::from_func(f);
    let solution = simplex_default(&mut caller, init);
    println!(
        "simplex\nx_min = {}, f(x_min) = {} with {} calls",
        solution,
        caller.apply(&solution),
        caller.call_count() - 1
    );
}

pub fn run_hooke_jeeves(init: &RealVec, f: ScalarFun) {
    let mut caller = FnCaller::from_func(f);
    let solution = hooke_jeeves(init, &mut caller, None, None);
    println!(
        "hooke_jeeves\nx_min = {}, f(x_min) = {} with {} calls",
        solution,
        caller.apply(&solution),
        caller.call_count() - 1
    );
}

pub fn run_coordinate_search(init: &RealVec, f: ScalarFun) {
    let mut caller = FnCaller::from_func(f);
    let solution = coordinate_search(init, &mut caller, None);
    println!(
        "coordinate_search\nx_min = {}, f(x_min) = {} with {} calls",
        solution,
        caller.apply(&solution),
        caller.call_count() - 1
    );
}

pub fn run_line(init: &RealVec, f: ScalarFun) {
    let mut caller = FnCaller::from_func(f);
    let solution = golden_cut(init, None, &mut caller, None, None, 0);
    println!(
        "golden_cut\nx_min = {}, f(x_min) = {} with {} calls",
        solution,
        caller.apply(&solution),
        caller.call_count() - 1
    );
}

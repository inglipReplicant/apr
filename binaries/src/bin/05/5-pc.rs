extern crate linalg;
extern crate numerical;

use linalg::matrix::RealMat;
use numerical::integration::*;

fn main() {
    let a = RealMat::from_file("binaries/files/05/pc_a.txt").unwrap();
    let x = RealMat::from_file("binaries/files/05/pc_x.txt").unwrap();
    let b = RealMat::from_dims(2, 2);
    let r = RealMat::from_dims(2, 1);

    let _ = predictor_corrector(&a, &x, &b, &r, 0.01, 10.0, 100, true, euler, inv_euler);
    println!("");
}

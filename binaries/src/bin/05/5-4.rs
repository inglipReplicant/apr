extern crate linalg;
extern crate numerical;

use linalg::matrix::RealMat;
use numerical::integration::*;

fn main() {
    let a = RealMat::from_file("binaries/files/05/4_a.txt").unwrap();
    let x = RealMat::from_file("binaries/files/05/4_x.txt").unwrap();
    let b = RealMat::from_file("binaries/files/05/4_b.txt").unwrap();

    let step = 0.01;
    let t_max = 1.0;
    let print_step = 9;

    println!("Solving using Euler's method:");
    let mut x_k = x.clone();
    let mut t = 0.0;
    let mut print_ctr = 0;
    while t < t_max {
        let r = RealMat::from_vec(vec![vec![t], vec![t]]).unwrap();
        let x_dot = euler(&a, &x_k, &b, &r, step, step, print_step, false).unwrap();

        if print_ctr % print_step == 0 {
            println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
        }
        print_ctr += 1;
        t += step;
        x_k = x_dot;
    }
    println!("");

    println!("Solving using inverse Euler's method:");
    let mut x_k = x.clone();
    let mut t = 0.0;
    let mut print_ctr = 0;
    while t < t_max {
        let r = RealMat::from_vec(vec![vec![t], vec![t]]).unwrap();
        let x_dot = inv_euler(&a, &x_k, &b, &r, step, step, print_step, false).unwrap();

        if print_ctr % print_step == 0 {
            println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
        }
        print_ctr += 1;
        t += step;
        x_k = x_dot;
    }
    println!("");

    println!("Solving using trapezoidal method:");
    let mut x_k = x.clone();
    let mut t = 0.0;
    let mut print_ctr = 0;
    while t < t_max {
        let r = RealMat::from_vec(vec![vec![t], vec![t]]).unwrap();
        let x_dot = trapezoidal(&a, &x_k, &b, &r, step, step, print_step, false).unwrap();

        if print_ctr % print_step == 0 {
            println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
        }
        print_ctr += 1;
        t += step;
        x_k = x_dot;
    }
    println!("");

    println!("Solving using Runge-Kutta method:");
    let mut x_k = x.clone();
    let mut t = 0.0;
    let mut print_ctr = 0;
    while t < t_max {
        let r = RealMat::from_vec(vec![vec![t], vec![t]]).unwrap();
        let x_dot = runge_kutta(&a, &x_k, &b, &r, step, step, print_step, false).unwrap();

        if print_ctr % print_step == 0 {
            println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
        }
        print_ctr += 1;
        t += step;
        x_k = x_dot;
    }
    println!("");

    println!("Solving using PE(CE)2:");
    let mut x_k = x.clone();
    let mut t = 0.0;
    let mut print_ctr = 0;
    while t < t_max {
        let r = RealMat::from_vec(vec![vec![t], vec![t]]).unwrap();
        let x_dot = predictor_corrector(
            &a, &x_k, &b, &r, step, step, print_step, false, euler, inv_euler,
        )
        .unwrap();

        if print_ctr % print_step == 0 {
            println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
        }
        print_ctr += 1;
        t += step;
        x_k = x_dot;
    }
    println!("");

    println!("Solving using PECE:");
    let mut x_k = x.clone();
    let mut t = 0.0;
    let mut print_ctr = 0;
    while t < t_max {
        let r = RealMat::from_vec(vec![vec![t], vec![t]]).unwrap();
        let x_dot = predictor_corrector(
            &a,
            &x_k,
            &b,
            &r,
            step,
            step,
            print_step,
            false,
            euler,
            trapezoidal,
        )
        .unwrap();

        if print_ctr % print_step == 0 {
            println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
        }
        print_ctr += 1;
        t += step;
        x_k = x_dot;
    }
}

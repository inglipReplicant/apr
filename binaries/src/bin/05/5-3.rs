extern crate linalg;
extern crate numerical;

use linalg::matrix::RealMat;
use numerical::integration::*;

fn main() {
    let a = RealMat::from_file("binaries/files/05/3_a.txt").unwrap();
    let x = RealMat::from_file("binaries/files/05/3_x.txt").unwrap();
    let b = RealMat::from_file("binaries/files/05/3_b.txt").unwrap();
    let r = RealMat::from_file("binaries/files/05/3_r.txt").unwrap();

    println!("Solving using Euler's method:");
    let _ = euler(&a, &x, &b, &r, 0.01, 10.0, 100, true);
    println!("");

    println!("Solving using inverse Euler's method:");
    let _ = inv_euler(&a, &x, &b, &r, 0.01, 10.0, 100, true);
    println!("");

    println!("Solving using trapezoidal method:");
    let _ = trapezoidal(&a, &x, &b, &r, 0.01, 10.0, 100, true);
    println!("");

    println!("Solving using Runge-Kutta method:");
    let _ = runge_kutta(&a, &x, &b, &r, 0.01, 10.0, 100, true);
    println!("");

    println!("Solving using PE(CE)2:");
    let _ = predictor_corrector(&a, &x, &b, &r, 0.01, 10.0, 100, true, euler, inv_euler);
    println!("");

    println!("Solving using PECE:");
    let _ = predictor_corrector(&a, &x, &b, &r, 0.01, 10.0, 100, true, euler, trapezoidal);
}

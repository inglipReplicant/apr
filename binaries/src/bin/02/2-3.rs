extern crate linalg;
extern crate optimi;

use linalg::vector::RealVec;
use optimi::functions::*;
use optimi::util::*;

pub fn main() {
    let init = RealVec::from_vec(vec![5.0, 5.0]);
    run_hooke_jeeves(&init, f_4);
    run_simplex(&init, f_4);
}

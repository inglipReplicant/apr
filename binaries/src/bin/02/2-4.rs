extern crate linalg;
extern crate optimi;

use linalg::vector::RealVec;
use optimi::functions::*;
use optimi::methods::simplex_custom;

pub fn main() {
    let x_init = RealVec::from_vec(vec![0.5, 0.5]);
    for i in 1..=20 {
        println!(
            "Solving function 1 with Nelder-Mead Simplex with step size: {}",
            i as f64
        );
        let mut f = FnCaller::from_func(f_1);
        let solution = simplex_custom(
            &mut f,
            &x_init,
            Some(i as f64),
            None,
            None,
            None,
            None,
            None,
        );
        println!(
            "x_min = {}, f(x_min) = {} with {} calls\n",
            solution,
            f.apply(&solution),
            f.call_count()
        );
    }

    println!("===== NOW SOLVING WITH X_INIT = (20, 20) =====\n");

    let x_init = RealVec::from_vec(vec![20.0, 20.0]);
    for i in 1..=20 {
        println!(
            "Solving function 1 with Nelder-Mead Simplex with step size: {}",
            i as f64
        );
        let mut f = FnCaller::from_func(f_1);
        let solution = simplex_custom(
            &mut f,
            &x_init,
            Some(i as f64),
            None,
            None,
            None,
            None,
            None,
        );
        println!(
            "x_min = {}, f(x_min) = {} with {} calls\n",
            solution,
            f.apply(&solution),
            f.call_count()
        );
    }
}

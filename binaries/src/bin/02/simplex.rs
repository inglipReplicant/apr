extern crate linalg;
extern crate optimi;

use linalg::vector::RealVec;
use optimi::functions::*;
use optimi::util::*;

pub fn main() {
    let init = RealVec::from_vec(vec![100.0]);
    run_simplex(&init, f_3a);
}

extern crate linalg;
extern crate optimi;

use linalg::vector::RealVec;
use optimi::functions::*;
use optimi::util::*;

pub fn main() {
    let init = RealVec::from_vec(vec![-1.9, 2.0]);
    run_hooke_jeeves(&init, f_1);
    run_simplex(&init, f_1);
    run_coordinate_search(&init, f_1);
    println!();

    let init = RealVec::from_vec(vec![0.1, 0.3]);
    run_hooke_jeeves(&init, f_2);
    run_simplex(&init, f_2);
    run_coordinate_search(&init, f_2);
    println!();

    let init = RealVec::from_vec(vec![0.0, 0.0, 0.0, 0.0, 0.0]);
    run_hooke_jeeves(&init, f_3);
    run_simplex(&init, f_3);
    run_coordinate_search(&init, f_3);
    println!();

    let init = RealVec::from_vec(vec![5.1, 1.1]);
    run_hooke_jeeves(&init, f_4);
    run_simplex(&init, f_4);
    run_coordinate_search(&init, f_4);
    println!();
}

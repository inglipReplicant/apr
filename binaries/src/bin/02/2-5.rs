extern crate linalg;
extern crate optimi;
extern crate rand;

use linalg::vector::RealVec;
use optimi::functions::*;
use optimi::methods::*;
use rand::{thread_rng, Rng};

pub fn main() {
    for _ in 1..=20 {
        let a = thread_rng().gen_range(-50.0, 50.0);
        let b = thread_rng().gen_range(-50.0, 50.0);
        let x_init = RealVec::from_vec(vec![a, b]);
        println!("Solving Schaeffer function with Nelder-Mead Simplex");
        let mut f = FnCaller::from_func(f_5);
        let solution = simplex_default(&mut f, &x_init);
        println!(
            "x_min = {}, f(x_min) = {} with {} calls\n",
            solution,
            f.apply(&solution),
            f.call_count()
        );
    }
}

extern crate linalg;
extern crate optimi;

use linalg::vector::RealVec;
use optimi::functions::*;
use optimi::methods::*;

pub fn main() {
    for i in 0..10 {
        let init = RealVec::from_vec(vec![i as f64 * 10.0]);

        let mut caller = FnCaller::from_func(f_3a);
        let solution = golden_cut(&init, None, &mut caller, None, None, 0);
        println!(
            "golden_cut\nx_min = {}, f(x_min) = {} with {} calls",
            solution,
            caller.apply(&solution),
            caller.call_count()
        );

        let mut caller = FnCaller::from_func(f_3a);
        let solution = coordinate_search(&init, &mut caller, None);
        println!(
            "coordinate_search\nx_min = {}, f(x_min) = {} with {} calls",
            solution,
            caller.apply(&solution),
            caller.call_count()
        );

        let mut caller = FnCaller::from_func(f_3a);
        let solution = hooke_jeeves(&init, &mut caller, None, None);
        println!(
            "hooke_jeeves\nx_min = {}, f(x_min) = {} with {} calls",
            solution,
            caller.apply(&solution),
            caller.call_count()
        );

        let mut caller = FnCaller::from_func(f_3a);
        let solution = simplex_default(&mut caller, &init);
        println!(
            "simplex\nx_min = {}, f(x_min) = {} with {} calls",
            solution,
            caller.apply(&solution),
            caller.call_count()
        );
        println!();
    }
}

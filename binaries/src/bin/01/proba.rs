extern crate linalg;

use linalg::error::MathError;
use linalg::lup::*;
use linalg::matrix::RealMat;

type Result<T> = std::result::Result<T, MathError>;

fn main() -> Result<()> {
    for i in 1..=3 {
        let mut path = "binaries/files/01/proba/lu_decom_".to_owned();
        path.push_str(&i.to_string());
        path.push_str(".txt");
        check_lu(&path)?;
    }

    let a = RealMat::from_file("binaries/files/01/proba/lu_decom_1.txt")?;
    let b = RealMat::from_file("binaries/files/01/proba/lu_b_1.txt")?;
    let x = solve_lu(&a, &b)?;
    println!("Solution is: {}\n", x);

    let a = RealMat::from_file("binaries/files/01/proba/lu_decom_4.txt")?;
    let b = RealMat::from_file("binaries/files/01/proba/lu_b_4.txt")?;
    let x = solve_lu(&a, &b)?;
    println!("Solution is: {}\n", x);

    let a = RealMat::from_file("binaries/files/01/proba/lup_decom_1.txt")?;
    let b = RealMat::from_file("binaries/files/01/proba/lup_b_1.txt")?;
    let x = solve_lup(&a, &b)?;
    println!("Solution is: {}\n", x);

    Ok(())
}

fn check_lu(path: &str) -> Result<()> {
    let mut a = RealMat::from_file(path)?;
    lu_inplace(&mut a)?;
    println!("Post-LU: {}", a);
    println!();
    Ok(())
}

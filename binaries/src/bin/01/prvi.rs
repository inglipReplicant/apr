extern crate linalg;

use linalg::error::MathError;
use linalg::lup::*;
use linalg::matrix::RealMat;

type Result<T> = std::result::Result<T, MathError>;

fn main() -> Result<()> {
    solve_problem("2")?;
    solve_problem("3")?;
    solve_problem("4")?;
    solve_problem("5")?;
    solve_problem("6")?;
    solve_problem("6mod")?;

    let a = RealMat::from_file("binaries/files/01/labos/7.txt")?;
    let inv_a = lup_invert(&a);
    println!("Inverse of a: {:?}", inv_a);

    let a = RealMat::from_file("binaries/files/01/labos/8.txt")?;
    let inv_a = lup_invert(&a);
    println!("Inverse of a: {}", inv_a.unwrap());

    println!("");
    let a = RealMat::from_file("binaries/files/01/labos/9.txt")?;
    let det = lup_determinant(&a)?;
    println!("Determinant of a: {}", det);

    let a = RealMat::from_file("binaries/files/01/labos/10.txt")?;
    let det = lup_determinant(&a)?;
    println!("Determinant of a: {}", det);
    Ok(())
}

fn solve_problem(name: &str) -> Result<()> {
    let mut a_str = String::from("binaries/files/01/labos/");
    a_str.push_str(name);
    let mut b_str = a_str.clone();
    a_str.push_str("-a.txt");
    b_str.push_str("-b.txt");

    let a = RealMat::from_file(&a_str)?;
    let b = RealMat::from_file(&b_str)?;
    println!("Solving: {}\n", name);
    println!("Solving using LU-Decomposition:");
    let x = solve(&a, &b, false);
    if x.is_ok() {
        println!("Solution is: {}\n", x.unwrap());
    } else {
        println!("{:?}", x);
    }

    println!("\nSolving using LUP-Decomposition:");
    let x = solve(&a, &b, true);
    if x.is_ok() {
        println!("Solution is: {}\n", x.unwrap());
    } else {
        println!("{:?}\n", x);
    }

    println!("==========================\n");
    Ok(())
}

extern crate linalg;
extern crate optimi;

use linalg::vector::RealVec;
use optimi::functions::*;
use optimi::methods::*;

pub fn main() {
    let x0 = RealVec::from_vec(vec![-1.9, 2.0]);
    let mut f1 = FnCaller::with_gradients(lab3_fn1, lab3_der1, lab3_hes1);

    let x1 = RealVec::from_vec(vec![0.1, 0.3]);
    let mut f2 = FnCaller::with_gradients(lab3_fn2, lab3_der2, lab3_hes2);

    let x_best = newton_raphson(&mut f1, &x0, true, 1e-3).unwrap();
    println!("Best: {}", x_best);
    f1.print_calls();
    f1.reset_count();
    println!();

    let x_best = newton_raphson(&mut f1, &x0, false, 1e-3).unwrap();
    println!("Best: {}", x_best);
    f1.print_calls();
    f1.reset_count();
    println!();

    let x_best = newton_raphson(&mut f2, &x1, true, 1e-3).unwrap();
    println!("Best: {}", x_best);
    f2.print_calls();
    f2.reset_count();
    println!();

    let x_best = newton_raphson(&mut f2, &x1, false, 1e-3).unwrap();
    println!("Best: {}", x_best);
    f2.print_calls();
    f2.reset_count();
}

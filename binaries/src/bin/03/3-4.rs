extern crate linalg;
extern crate optimi;

use linalg::vector::{RealVec, Vector};
use optimi::functions::*;
use optimi::transformer::{MixedTransform, mixed_transform};

fn limit_1(x: &RealVec) -> f64 {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();
    x2 - x1
}

fn limit_2(x: &RealVec) -> f64 {
    let x1 = x.get_elem(0).unwrap();
    2.0 - x1
}

fn domain_limit_100(x: &RealVec) -> f64 {
    let (lower, upper) = (-100.0, 100.0);
    let xs = x.values();
    for v in xs.iter() {
        if *v < lower || *v > upper {
            return -1.0;
        }
    }
    0.0
}

pub fn main() {
    let x0 = RealVec::from_vec(vec![-1.9, 2.0]);
    let mut f1 = FnCaller::with_gradients(lab3_fn1, lab3_der1, lab3_hes1);
    let mut lim1 = FnCaller::from_func(limit_1);
    let mut lim2 = FnCaller::from_func(limit_2);
    let mut h1 = FnCaller::from_func(domain_limit_100);
    let gs = vec![&mut lim1, &mut lim2];
    let hs = vec![&mut h1];

    let mut transformer = MixedTransform::new(&mut f1, gs, hs, None);
    let best = mixed_transform(&mut transformer, &x0, 1.0, 1e-6);
    println!("Found best: {}", best);
    println!();

    let x1 = RealVec::from_vec(vec![0.1, 0.3]);
    let mut f2 = FnCaller::with_gradients(lab3_fn2, lab3_der2, lab3_hes2);
    let mut lim1 = FnCaller::from_func(limit_1);
    let mut lim2 = FnCaller::from_func(limit_2);
    let mut h1 = FnCaller::from_func(domain_limit_100);
    let gs = vec![&mut lim1, &mut lim2];
    let hs = vec![&mut h1];
    let mut transformer = MixedTransform::new(&mut f2, gs, hs, None);
    let best = mixed_transform(&mut transformer, &x1, 1.0, 1e-6);
    println!("Found best: {}", best);
}

extern crate linalg;
extern crate optimi;

use linalg::vector::{RealVec, Vector};
use optimi::functions::*;
use optimi::transformer::{MixedTransform, mixed_transform};

fn limit_1(x: &RealVec) -> f64 {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();
    3.0 - x1 - x2
}

fn limit_2(x: &RealVec) -> f64 {
    let x1 = x.get_elem(0).unwrap();
    let x2 = x.get_elem(1).unwrap();
    3.0 + 1.5 * x1 - x2
}

fn domain_limit_100(x: &RealVec) -> f64 {
    let x2 = x.get_elem(1).unwrap();
    x2 - 1.0
}

pub fn main() {
    let x0 = RealVec::from_vec(vec![5.0, 5.0]);
    let mut f1 = FnCaller::with_gradients(lab3_fn4, lab3_der4, lab3_hes4);
    let mut lim1 = FnCaller::from_func(limit_1);
    let mut lim2 = FnCaller::from_func(limit_2);
    let mut h1 = FnCaller::from_func(domain_limit_100);
    let gs = vec![&mut lim1, &mut lim2];
    let hs = vec![&mut h1];

    let mut transformer = MixedTransform::new(&mut f1, gs, hs, None);
    let best = mixed_transform(&mut transformer, &x0, 1.0, 1e-6);
    println!("Found best: {}", best);
    println!();
}

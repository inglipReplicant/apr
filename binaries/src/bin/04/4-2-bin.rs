extern crate evo;
extern crate rand;

use evo::apr_binary::*;
use evo::apr_functions::*;
use evo::fitness::FitnessFunction;
use evo::ops::*;
use evo::population::Population;
use evo::unit::Unit;
use rand::thread_rng;

fn main() {
    println!("Solving the problem with eliminative 3-tournament algorithm.");

    let mut fun = GeneticCaller::from_func(f_1);
    let pop_size = 300;
    let prob_dimension = 2;
    let mut_prob = 0.05;
    let _elim_coef = pop_size / 2;
    let max_generations = 100000;
    let num_parents = 3;
    let err_margin = 1e-9;
    let single_len = 10;
    let decimal_pts = 4;

    let mut rng = thread_rng();

    let selector = BinaryTourney::new(num_parents);
    let breeder = UniformCrossover {};
    let mutator = BinaryMutator {
        mutation_probability: mut_prob,
    };

    let bound = Bound::new(-50.0, 150.0);
    let mut best_fit = fun.fitness_max();
    let mut pop = generate_initial_binary(
        pop_size,
        prob_dimension,
        single_len,
        decimal_pts,
        &bound,
        &mut fun,
    );

    fun.update_evaluate_binary_pop(&mut pop);
    pop.sort_ascending();
    for i in 0..max_generations {
        let best = pop.units().get(0).unwrap();

        if best.get_fitness_scalar() < best_fit {
            best_fit = best.get_fitness_scalar();
            println!("Iteration {}, new best unit is {:?}", i, best);
        } else if best.get_fitness_scalar() < err_margin {
            println!("Found best in iter {}: {:?}", i, best);
            break;
        }

        let three_parents = selector.select_from(&pop, &mut rng);
        let mut parent_pop = Population::from_units(three_parents);
        fun.update_evaluate_binary_pop(&mut parent_pop);
        parent_pop.sort_ascending();

        let child;
        {
            let p1 = parent_pop.units().get(0).unwrap();
            let p2 = parent_pop.units().get(1).unwrap();
            child = breeder.crossover(vec![p1.clone(), p2.clone()], &mut rng)[0].clone();
        }
        let mut child = mutator.mutate(child, &mut rng);
        let fit = fun.fitness_of(&child);
        child.update_fitness(fit);

        let third = parent_pop.units().get(2).unwrap();
        if child.get_fitness_scalar() < third.get_fitness_scalar() {
            for unit in pop.units_mut().iter_mut() {
                if (unit.get_fitness_scalar() - third.get_fitness_scalar()).abs() < err_margin {
                    unit.update_chromosome(child.get_chromo().clone());
                    unit.update_fitness(*child.get_fitness());
                }
            }
        }
    }
}

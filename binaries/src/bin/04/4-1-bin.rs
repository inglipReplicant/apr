extern crate evo;
extern crate rand;

use evo::apr_binary::*;
use evo::apr_functions::*;
use evo::fitness::FitnessFunction;
use evo::ops::*;
use evo::population::Population;
use evo::unit::Unit;
use rand::thread_rng;

fn main() {
    println!("Solving the problem with generative genetic algorithm.");

    let mut fun = GeneticCaller::from_func(f_1);
    let pop_size = 50;
    let prob_dimension = 2;
    let mut_prob = 0.25;
    let elitism_coef = 2;
    let single_len = 10;
    let decimal_pts = 4;
    //let _elim_coef = pop_size / 2;
    let max_generations = 100_000;
    let err_margin = 1e-9;

    let bound = Bound::new(-50.0, 150.0);

    let mut rng = thread_rng();

    let selector = BinaryRoulette {};
    let breeder = UniformCrossover {};
    let mutator = BinaryMutator {
        mutation_probability: mut_prob,
    };

    let mut best_fit = fun.fitness_max();
    let mut pop = generate_initial_binary(
        pop_size,
        prob_dimension,
        single_len,
        decimal_pts,
        &bound,
        &mut fun,
    );
    for i in 0..max_generations {
        fun.update_evaluate_binary_pop(&mut pop);
        pop.sort_ascending();
        let best = pop.units().get(0).unwrap();

        if best.get_fitness_scalar() < best_fit {
            best_fit = best.get_fitness_scalar();
            println!("Iteration {}, new best unit is {:?}", i, best);
        } else if best.get_fitness_scalar() < err_margin {
            println!("Found best in iter {}: {:?}", i, best);
            break;
        }

        let mut next_gen = Vec::new();
        for i in 0..elitism_coef {
            next_gen.push(pop.units().get(i).unwrap().clone());
        }

        pop.sort_normalize();
        //let sum = pop.fitness_sum();

        let new_units = pop_size - elitism_coef;
        for _ in 0..new_units {
            let fst_parent = selector.select_from(&pop, &mut rng)[0].clone();
            let snd_parent = selector.select_from(&pop, &mut rng)[0].clone();

            let child = breeder.crossover(vec![fst_parent, snd_parent], &mut rng);
            let child = mutator.mutate(child[0].clone(), &mut rng);
            next_gen.push(child);
        }
        pop = Population::from_units(next_gen);
    }
}

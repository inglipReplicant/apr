extern crate linalg;

use linalg::{
    error::MathError,
    lup::lup_invert,
    matrix::{Matrix, RealMat},
};

type Result<T> = std::result::Result<T, MathError>;

pub fn euler(
    a: &RealMat,
    x: &RealMat,
    b: &RealMat,
    r: &RealMat,
    step: f64,
    t_max: f64,
    print_step: usize,
    trace: bool,
) -> Result<RealMat> {
    let mut t = 0.0;
    let mut x_k = x.clone();

    while t < t_max {
        let sum = a.mul(&x_k)?.add(&b.mul(r)?)?;
        let x_dot = x_k.add(&sum.scale(step)?)?;

        if (t / step) as usize % print_step == 0 {
            if trace {
                println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
            }
        }
        t += step;
        x_k = x_dot;
    }
    Ok(x_k)
}

pub fn inv_euler(
    a: &RealMat,
    x: &RealMat,
    b: &RealMat,
    r: &RealMat,
    step: f64,
    t_max: f64,
    print_step: usize,
    trace: bool,
) -> Result<RealMat> {
    let mut t = 0.0;
    let mut x_k = x.clone();

    let u = RealMat::id(a.dim_cols());
    let p = lup_invert(&u.sub(&a.scale(step)?)?)?;
    let q = p.scale(step)?.mul(b)?;

    while t < t_max {
        let x_dot = p.mul(&x_k)?.add(&q.mul(r)?)?;

        if (t / step) as usize % print_step == 0 {
            if trace {
                println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
            }
        }
        t += step;
        x_k = x_dot;
    }
    //println!("Returning: {}", x_k);
    Ok(x_k)
}

pub fn trapezoidal(
    a: &RealMat,
    x: &RealMat,
    b: &RealMat,
    r: &RealMat,
    step: f64,
    t_max: f64,
    print_step: usize,
    trace: bool,
) -> Result<RealMat> {
    let mut t = 0.0;
    let mut x_k = x.clone();

    let u = RealMat::id(a.dim_cols());
    let inv = lup_invert(&(u.sub(&a.scale(0.5 * step)?)?))?;
    let q = inv.mul(&(u.add(&a.scale(0.5 * step)?)?))?;
    let s = inv.scale(0.5 * step)?.mul(b)?;

    while t < t_max {
        let x_dot = q.mul(&x_k)?.add(&s.mul(&(&r.add(&r)?))?)?;

        if (t / step) as usize % print_step == 0 {
            if trace {
                println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
            }
        }
        t += step;
        x_k = x_dot;
    }
    Ok(x_k)
}

pub fn runge_kutta(
    a: &RealMat,
    x: &RealMat,
    b: &RealMat,
    r: &RealMat,
    step: f64,
    t_max: f64,
    print_step: usize,
    trace: bool,
) -> Result<RealMat> {
    let mut t = 0.0;
    let mut x_k = x.clone();
    let b_rk = b.mul(r)?;

    while t < t_max {
        let m1 = a.mul(&x_k)?.add(&b_rk)?;
        let t1 = x_k.add(&m1.scale(0.5 * step)?)?;
        let m2 = a.mul(&t1)?.add(&b_rk)?;
        let t2 = x_k.add(&m2.scale(0.5 * step)?)?;
        let m3 = a.mul(&t2)?.add(&b_rk)?;
        let t3 = x_k.add(&m3.scale(step)?)?;
        let m4 = a.mul(&t3)?.add(&b_rk)?;
        let sum = m1.add(&m2.scale(2.0)?)?.add(&m3.scale(2.0)?)?.add(&m4)?;
        let x_dot = sum.scale(step / 6.0)?.add(&x_k)?;

        if (t / step) as usize % print_step == 0 {
            if trace {
                println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
            }
        }
        t += step;
        x_k = x_dot;
    }
    Ok(x_k)
}

pub fn predictor_corrector<F, G>(
    a: &RealMat,
    x: &RealMat,
    b: &RealMat,
    r: &RealMat,
    step: f64,
    t_max: f64,
    print_step: usize,
    trace: bool,
    predictor: F,
    corrector: G,
) -> Result<RealMat>
where
    F: Fn(&RealMat, &RealMat, &RealMat, &RealMat, f64, f64, usize, bool) -> Result<RealMat>,
    G: Fn(&RealMat, &RealMat, &RealMat, &RealMat, f64, f64, usize, bool) -> Result<RealMat>,
{
    let mut t = 0.0;
    let mut x_k = x.clone();

    while t < t_max {
        let x_predicted = predictor(a, &x_k, b, r, step, step, 10, false)?;
        let x_dot = corrector(a, &x_predicted, b, r, step, step, 10, false)?;
        //println!("x_dot = {}", x_dot);

        if (t / step) as usize % print_step == 0 {
            if trace {
                println!("t = {} | x_k = {} | x_k+1 = {}", t, x_k, x_dot);
            }
        }
        t += step;
        x_k = x_dot;
    }
    Ok(x_k)
}

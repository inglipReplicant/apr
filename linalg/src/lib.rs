pub mod error;
pub mod lup;
pub mod matrix;
pub mod vector;

pub static PRECISION: f64 = 1e-9; //floating point precision for float comparison

use crate::error::MathError;
use crate::matrix::{Matrix, RealMat};
use crate::vector::RealVec;
use crate::PRECISION;

type Result<T> = std::result::Result<T, MathError>;

fn check_dims_lu(mat: &RealMat) -> Result<()> {
    if !mat.is_square() {
        return Err(MathError::DimensionError);
    }
    let n = mat.dim_rows();
    for i in 0..n {
        if mat.elem_zero(i, i) {
            return Err(MathError::ZeroElementError);
        }
    }
    Ok(())
}

/// Computes the forward substitution of a matrix system, if possible.
pub fn subst_forward(a: &RealMat, b: &RealMat) -> Result<RealMat> {
    check_dims_lu(a)?;
    let n = a.dim_rows();
    let mut ys = b.clone();

    for i in 0..(n - 1) {
        for j in (i + 1)..n {
            let new = a.get_elem(j, i)? * ys.get_elem(i, 0)?;
            ys.sub_elem(j, 0, new)?;
        }
    }
    Ok(ys)
}

pub fn subst_back(a: &RealMat, b: &RealMat) -> Result<RealMat> {
    check_dims_lu(a)?;

    let n = a.dim_rows();
    let mut xs = vec![0.0; n];
    for i in (0..n).rev() {
        let mut sum = 0.0;
        for j in (i + 1)..n {
            sum += a.get_elem(i, j)? * xs[j];
        }
        let b = b.get_elem(i, 0).expect("Y vector is not a column vector.");
        let e = (b - sum) / a.get_elem(i, i)?;
        xs[i] = e;
    }
    let cvec = RealVec::from_vec(xs);
    let x = RealMat::from_vec_into_col(&cvec)?;
    Ok(x)
}

/// Decomposes a matrix inplace.
pub fn lu_inplace(a: &mut RealMat) -> Result<()> {
    check_dims_lu(a)?;
    let n = a.dim_rows();

    for i in 0..(n - 1) {
        if a.elem_zero(i, i) {
            return Err(MathError::ZeroElementError);
        }

        for j in (i + 1)..n {
            a.div_elem(j, i, a.get_elem(i, i)?)?;
            for k in (i + 1)..n {
                let new = a.get_elem(j, i)? * a.get_elem(i, k)?;
                //println!("element before subtraction: {} with {}", a.get_elem(j, k)?, new);
                a.sub_elem(j, k, new)?;
                //println!("element after subtraction: {}", a.get_elem(j, k)?);
            }
        }
    }
    Ok(())
}

pub fn lup_inplace(a: &mut RealMat) -> Result<(RealMat, Vec<usize>)> {
    check_dims_lu(a)?;
    let n = a.dim_rows();
    let mut p = (0..n + 1).collect::<Vec<usize>>();
    let mut pmat = RealMat::id(n);

    for i in 0..n {
        let mut max_a = 0.0;
        let mut imax = i;

        for k in i..n {
            let abs_a = a.get_elem(k, i)?.abs();
            if abs_a > max_a {
                max_a = abs_a;
                imax = k;
            }
        }

        if max_a < PRECISION {
            return Err(MathError::ZeroElementError);
        }

        if imax != i {
            let temp = p[i];
            p[i] = p[imax];
            p[imax] = temp;

            a.swap_rows_inplace(i, imax)?;
            pmat.swap_rows_inplace(i, imax)?;

            p[n] += 1;
        }

        for j in (i + 1)..n {
            a.div_elem(j, i, a.get_elem(i, i)?)?;
            for k in (i + 1)..n {
                let new = a.get_elem(j, i)? * a.get_elem(i, k)?;
                a.sub_elem(j, k, new)?;
            }
        }
    }
    Ok((pmat, p))
}

pub fn gen_lu(a: &RealMat) -> Result<(RealMat, RealMat)> {
    let mut l = a.clone();
    let mut u = a.clone();
    let n = a.dim_rows();

    for i in 0..n {
        for j in 0..n {
            if j > i {
                l.set_elem(i, j, 0.0)?;
            } else if j == i {
                l.set_elem(i, j, 1.0)?;
            }
            if j < i {
                u.set_elem(i, j, 0.0)?;
            }
        }
    }

    Ok((l, u))
}

pub fn lup_invert(a: &RealMat) -> Result<RealMat> {
    let mut a = a.clone();
    let n = a.dim_rows();
    let mut inv_a = RealMat::diag(0.0, n);
    let (_, p) = lup_inplace(&mut a)?;

    for j in 0..n {
        for i in 0..n {
            if p[i] == j {
                inv_a.set_elem(i, j, 1.0)?;
            } else {
                inv_a.set_elem(i, j, 0.0)?;
            }

            for k in 0..i {
                let new = a.get_elem(i, k)? * inv_a.get_elem(k, j)?;
                inv_a.sub_elem(i, j, new)?;
            }
        }

        for i in (0..n).rev() {
            for k in (i + 1)..n {
                let new = a.get_elem(i, k)? * inv_a.get_elem(k, j)?;
                inv_a.sub_elem(i, j, new)?;
            }
            inv_a.div_elem(i, j, a.get_elem(i, i)?)?;
        }
    }
    Ok(inv_a)
}

pub fn lup_determinant(a: &RealMat) -> Result<f64> {
    let mut lu = a.clone();
    let (_, p) = lup_inplace(&mut lu)?;
    let mut det = lu.get_elem(0, 0)?;
    let n = a.dim_cols();

    for i in 1..n {
        det *= lu.get_elem(i, i)?;
    }
    if (p[n] - n) % 2 == 0 {
        return Ok(det);
    }
    Ok(-1.0 * det)
}

pub fn solve(a: &RealMat, b: &RealMat, use_lup: bool) -> Result<RealMat> {
    match use_lup {
        true => solve_lup(a, b),
        false => solve_lu(a, b),
    }
}

pub fn solve_lu(a: &RealMat, b: &RealMat) -> Result<RealMat> {
    let mut lu = a.clone();
    println!("Pre-LU: A = {}, B = {}", a, b);
    lu_inplace(&mut lu)?;
    println!("Post-LU: {}", lu);
    let (l, u) = gen_lu(&lu)?;
    println!("L: {}", l);
    println!("U: {}", u);
    let y = subst_forward(&l, b)?;
    println!("Y: {}", y);
    let x = subst_back(&u, &y)?;
    println!("X: {}", x);
    Ok(x)
}

pub fn solve_lup(a: &RealMat, b: &RealMat) -> Result<RealMat> {
    let mut lu = a.clone();
    println!("Pre-LUP: A = {}, B = {}", a, b);
    let (p, _) = lup_inplace(&mut lu)?;
    let pb = &p * b;
    println!("Post-LUP: lu = {}\np = {}\npb = {}", lu, p, pb);
    let (l, u) = gen_lu(&lu)?;
    println!("L: {}", l);
    println!("U: {}", u);
    let c = subst_forward(&l, &pb)?;
    println!("c: {}", c);
    let x = subst_back(&u, &c)?;
    println!("x: {}", x);
    Ok(x)
}

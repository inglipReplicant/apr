#[derive(Debug)]
pub enum MathError {
    InvalidIndexError,
    DimensionError,
    ZeroElementError,
}

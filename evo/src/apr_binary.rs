/// Real valued chromosomes and corresponding mechanisms
use crate::{
    apr_functions::{Applicable, GeneticCaller},
    fitness::{Fitness, FitnessFunction},
    ops::*,
    population::Population,
    unit::Unit,
};

use rand::distributions::Bernoulli;
use rand::{thread_rng, Rng};

fn decode_bits(lower: f64, upper: f64, bits: &[bool], decimal_pts: usize) -> f64 {
    let len = bits.len();

    let mut sum = 0.0;
    let mut decim = 0.5;
    for i in 0..len {
        if i > len - decimal_pts {
            sum += (bits[i] as i32) as f64 * decim;
            decim /= 2.0;
        } else {
            sum += sum * 2.0 + (bits[i] as i32) as f64;
        }
    }
    //println!("SUM {}", sum);
    let n = 2.0_f64.powi(bits.len() as i32) - 1.0;
    lower + (sum / n) * (upper - lower)
}

#[derive(Clone, Debug, PartialEq)]
pub struct BinaryUnit {
    chromosome: Vec<bool>,

    single_len: usize,

    decimal_pts: usize,

    reals: Vec<f64>,

    fitness: f64,

    bound: Bound,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Bound(f64, f64);

impl Bound {
    pub fn new(lower: f64, upper: f64) -> Bound {
        Bound(lower, upper)
    }
}

impl BinaryUnit {
    pub fn new(
        chromosome: Vec<bool>,
        fitness: f64,
        single_len: usize,
        decimal_pts: usize,
        bound: &Bound,
    ) -> BinaryUnit {
        let reals = chromosome
            .chunks(single_len)
            .map(|bits| decode_bits(bound.0, bound.1, bits, decimal_pts))
            .collect::<Vec<f64>>();
        BinaryUnit {
            chromosome: chromosome,
            single_len: single_len,
            decimal_pts: decimal_pts,
            reals: reals,
            fitness: fitness,
            bound: bound.clone(),
        }
    }
}

impl Unit for BinaryUnit {
    type Chromosome = Vec<bool>;
    type Fitness = f64;

    fn get_chromo(&self) -> &Vec<bool> {
        &self.chromosome
    }

    fn get_fitness(&self) -> &f64 {
        &self.fitness
    }

    fn update_fitness(&mut self, fit: f64) {
        self.fitness = fit;
    }

    fn get_fitness_scalar(&self) -> f64 {
        self.fitness.to_scalar()
    }

    fn update_chromosome(&mut self, chromosome: Vec<bool>) {
        let reals = chromosome
            .chunks(self.single_len)
            .map(|bits| decode_bits(self.bound.0, self.bound.1, bits, self.decimal_pts))
            .collect::<Vec<f64>>();
        self.reals = reals;
        self.chromosome = chromosome;
    }
}

impl Applicable for BinaryUnit {
    fn as_floats(&self) -> &Vec<f64> {
        &self.reals
    }
}

impl FitnessFunction<BinaryUnit, f64> for GeneticCaller<BinaryUnit> {
    fn fitness_of(&mut self, x: &BinaryUnit) -> f64 {
        self.apply(x)
    }

    fn fitness_min(&self) -> f64 {
        0.0
    }

    fn fitness_max(&self) -> f64 {
        std::f64::MAX
    }
}

pub struct BinaryRoulette {}

impl SelectionOp<BinaryUnit, f64> for BinaryRoulette {
    fn select_from<R>(&self, current_gen: &Population<BinaryUnit>, rng: &mut R) -> Vec<BinaryUnit>
    where
        R: Rng + Sized,
    {
        let len = current_gen.size();
        let mut fit_sum = 0.0;
        let mut max_value = 0.0;
        for unit in current_gen.units().iter() {
            let fit = unit.get_fitness_scalar();
            fit_sum += fit;
            if fit > max_value {
                max_value = fit;
            }
        }

        fit_sum = (len as f64) * max_value - fit_sum;
        let rnum = rng.gen_range(0.0, 1.0) * fit_sum;
        let mut accumlated = 0.0;

        for (i, unit) in current_gen.units().iter().enumerate() {
            let fit = unit.get_fitness_scalar();
            accumlated += max_value - fit;
            if rnum < accumlated {
                return vec![current_gen.units().get(i).unwrap().clone()];
            }
        }
        return vec![current_gen.units().get(len - 1).unwrap().clone()];
    }
}

pub struct BinaryTourney {
    k: usize,
}

impl BinaryTourney {
    pub fn new(k: usize) -> BinaryTourney {
        BinaryTourney { k: k }
    }
}

impl SelectionOp<BinaryUnit, f64> for BinaryTourney {
    fn select_from<R>(&self, current_gen: &Population<BinaryUnit>, rng: &mut R) -> Vec<BinaryUnit>
    where
        R: Rng + Sized,
    {
        let mut candidates = Vec::new();
        for _ in 0..self.k {
            let index = rng.gen_range(0, current_gen.size());
            candidates.push(current_gen.units().get(index).unwrap().clone());
        }
        candidates
    }
}

pub struct SinglePointCrossover {}

impl CrossoverOp<BinaryUnit> for SinglePointCrossover {
    fn crossover<R>(&self, parents: Vec<BinaryUnit>, rng: &mut R) -> Vec<BinaryUnit>
    where
        R: Rng + Sized,
    {
        let parent = parents.get(0).unwrap();
        let fst = parents.get(0).unwrap().get_chromo();
        let snd = parents.get(1).unwrap().get_chromo();
        let len = fst.len();
        let mut first_child = vec![false; len];
        let mut second_child = vec![false; len];

        let index = rng.gen_range(0, len);
        for i in 0..len {
            if i <= index {
                first_child[i] = fst[i];
                second_child[i] = snd[i];
            } else {
                first_child[i] = snd[i];
                second_child[i] = fst[i];
            }
        }
        let c1 = BinaryUnit::new(
            first_child,
            0.0,
            parent.single_len,
            parent.decimal_pts,
            &parent.bound,
        );
        let c2 = BinaryUnit::new(
            second_child,
            0.0,
            parent.single_len,
            parent.decimal_pts,
            &parent.bound,
        );
        vec![c1, c2]
    }
}

pub struct UniformCrossover {}

impl CrossoverOp<BinaryUnit> for UniformCrossover {
    fn crossover<R>(&self, parents: Vec<BinaryUnit>, _: &mut R) -> Vec<BinaryUnit>
    where
        R: Rng + Sized,
    {
        let parent = parents.get(0).unwrap();
        let fst = parents.get(0).unwrap().get_chromo();
        let snd = parents.get(1).unwrap().get_chromo();
        let len = fst.len();
        let mut first_child = vec![false; len];
        let mut second_child = vec![false; len];
        let mut rng = thread_rng();
        let distribution = Bernoulli::new(0.5).unwrap();

        let mut r = vec![false; len];
        for i in 0..len {
            r[i] = rng.sample(&distribution);
        }

        for i in 0..len {
            let d1 = fst[i] && snd[i] || (r[i]) && (fst[i] ^ snd[i]);
            let d2 = fst[i] && snd[i] || (!r[i]) && (fst[i] ^ snd[i]);
            first_child[i] = d1;
            second_child[i] = d2;
        }

        let c1 = BinaryUnit::new(
            first_child,
            0.0,
            parent.single_len,
            parent.decimal_pts,
            &parent.bound,
        );
        let c2 = BinaryUnit::new(
            second_child,
            0.0,
            parent.single_len,
            parent.decimal_pts,
            &parent.bound,
        );
        vec![c1, c2]
    }
}

pub struct BinaryMutator {
    pub mutation_probability: f64,
}

impl MutationOp<BinaryUnit> for BinaryMutator {
    fn mutate<R>(&self, unit: BinaryUnit, rng: &mut R) -> BinaryUnit
    where
        R: Rng + Sized,
    {
        let len = unit.get_chromo().len();
        let mut new = unit.get_chromo().clone();

        for i in 0..len {
            if rng.gen_range(0.0, 1.0) < self.mutation_probability {
                let nbool = rng.gen_bool(0.5);
                new[i] = nbool;
            }
        }
        BinaryUnit::new(new, 0.0, unit.single_len, unit.decimal_pts, &unit.bound)
    }
}

pub fn generate_initial_binary(
    pop_size: usize,
    problem_dimension: usize,
    single_len: usize,
    decimal_pts: usize,
    bound: &Bound,
    evaluator: &mut GeneticCaller<BinaryUnit>,
) -> Population<BinaryUnit> {
    let mut units = Vec::new();
    let mut rng = thread_rng();
    let distribution = Bernoulli::new(0.5).unwrap();

    for _ in 0..pop_size {
        let mut chromos: Vec<bool> = Vec::new();
        for _ in 0..(single_len * problem_dimension) {
            chromos.push(rng.sample(&distribution));
        }
        let mut unit = BinaryUnit::new(chromos, 0.0, single_len, decimal_pts, bound);
        let fit = evaluator.fitness_of(&unit);
        unit.update_fitness(fit);
        units.push(unit);
    }
    Population::from_units(units)
}

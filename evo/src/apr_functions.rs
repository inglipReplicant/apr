use crate::apr_binary::BinaryUnit;
use crate::apr_real::BoundedUnit;
use crate::population::Population;
use crate::unit::Unit;

pub trait Applicable {
    fn as_floats(&self) -> &Vec<f64>;
}

pub type GeneticFun<T> = fn(&T) -> f64;

#[derive(Clone)]
pub struct GeneticCaller<T: Applicable> {
    ctr: usize,

    f: GeneticFun<T>,
}

impl<T: Applicable> GeneticCaller<T> {
    pub fn from_func(f: GeneticFun<T>) -> GeneticCaller<T> {
        GeneticCaller { ctr: 0, f: f }
    }

    pub fn apply(&mut self, x: &T) -> f64 {
        self.ctr += 1;
        (self.f)(x)
    }

    pub fn reset_count(&mut self) {
        self.ctr = 0;
    }

    pub fn call_count(&self) -> usize {
        self.ctr
    }
}

impl GeneticCaller<BoundedUnit> {
    pub fn update_evaluate_real_pop(&mut self, pop: &mut Population<BoundedUnit>) {
        for unit in pop.units_mut() {
            let fit = self.apply(unit);
            unit.update_fitness(fit);
        }
    }
}

impl GeneticCaller<BinaryUnit> {
    pub fn update_evaluate_binary_pop(&mut self, pop: &mut Population<BinaryUnit>) {
        for unit in pop.units_mut() {
            let fit = self.apply(unit);
            unit.update_fitness(fit);
        }
    }
}

pub fn f_1<T: Applicable>(x: &T) -> f64 {
    let x = x.as_floats();
    let x1 = x[0];
    let x2 = x[1];
    100.0 * (x2 - x1.powi(2)).powi(2) + (1.0 - x1).powi(2)
}

pub fn f_3<T: Applicable>(x: &T) -> f64 {
    let x = x.as_floats();
    let n = x.len();
    x.iter()
        .zip(1..=n)
        .fold(0.0, |sum, (x_i, i)| sum + (x_i - i as f64).powi(2))
}

pub fn f_6<T: Applicable>(x: &T) -> f64 {
    let x = x.as_floats();
    let sum = x.iter().fold(0.0, |sum, x_i| sum + x_i.powi(2)); //Sum of squares
    0.5 + (sum.sqrt().sin().powi(2) - 0.5) / (1.0 + 0.001 * sum).powi(2)
}

pub fn f_7<T: Applicable>(x: &T) -> f64 {
    let x = x.as_floats();
    let sum = x.iter().fold(0.0, |sum, x_i| sum + x_i.powi(2)); //Sum of squares
    sum.powf(0.25) * (1.0 + (sum.powf(0.1) * 50.0).sin().powi(2))
}

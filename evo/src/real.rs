/// Real valued chromosomes and corresponding mechanisms
use crate::{
    fitness::{Fitness, FitnessFunction},
    ops::*,
    population::Population,
    unit::Unit,
};

use rand::{thread_rng, Rng};
use rand_distr::StandardNormal;

#[derive(Clone, Debug, PartialEq)]
pub struct RealUnit {
    chromosome: Vec<f64>,

    fitness: f64,
}

impl RealUnit {
    pub fn new(chromosome: Vec<f64>, fitness: f64) -> RealUnit {
        RealUnit {
            chromosome: chromosome,
            fitness: fitness,
        }
    }

    pub fn from_vec(chromosome: Vec<f64>) -> RealUnit {
        RealUnit {
            chromosome: chromosome,
            fitness: 0.0,
        }
    }
}

impl Unit for RealUnit {
    type Chromosome = Vec<f64>;
    type Fitness = f64;

    fn get_chromo(&self) -> &Vec<f64> {
        &self.chromosome
    }

    fn get_fitness(&self) -> &f64 {
        &self.fitness
    }

    fn update_fitness(&mut self, fit: f64) {
        self.fitness = fit;
    }

    fn get_fitness_scalar(&self) -> f64 {
        self.fitness.to_scalar()
    }

    fn update_chromosome(&mut self, chromosome: Vec<f64>) {
        self.chromosome = chromosome;
    }
}

#[derive(Clone, Debug)]
pub struct LabosRegression {
    pub xs: Vec<f64>,
    pub ys: Vec<f64>,
    pub zs: Vec<f64>,
}

impl LabosRegression {
    /// Evaluates the entire population and updates their corresponding fitness values.
    pub fn evaluate_update_population(&mut self, pop: &mut Population<RealUnit>) {
        for unit in pop.units_mut() {
            let fit = self.fitness_of(&unit);
            unit.update_fitness(fit);
        }
    }
}

impl FitnessFunction<RealUnit, f64> for LabosRegression {
    fn fitness_of(&mut self, x: &RealUnit) -> f64 {
        let example_len = self.xs.len();
        let betas = x.get_chromo();

        let mut sq_err = 0.0;
        for i in 0..example_len {
            let x = self.xs[i];
            let y = self.ys[i];
            let z_actual = self.zs[i];

            let z_calculated = (betas[0] + betas[1] * x).sin()
                + betas[2] * (x * (betas[3] + y)).cos() / (1.0 + (x - betas[4]).powi(2).exp());

            sq_err += (z_actual - z_calculated).powi(2);
        }
        sq_err / (example_len as f64)
    }

    fn fitness_min(&self) -> f64 {
        std::f64::MIN
    }

    fn fitness_max(&self) -> f64 {
        std::f64::MAX
    }
}

pub fn generate_initial_genetic(
    size: usize,
    solution_dim: usize,
    evaluator: &mut LabosRegression,
) -> Population<RealUnit> {
    let mut units = Vec::new();
    let mut rng = thread_rng();
    for _ in 0..size {
        let mut chromos = Vec::new();
        for _ in 0..solution_dim {
            chromos.push(rng.gen_range(-4.0, 4.0));
        }
        let mut unit = RealUnit::from_vec(chromos);
        let fit = evaluator.fitness_of(&unit);
        unit.update_fitness(fit);
        units.push(unit);
    }
    Population::from_units(units)
}

pub struct RouletteSelect {}

impl SelectionOp<RealUnit, f64> for RouletteSelect {
    fn select_from<R>(&self, current_gen: &Population<RealUnit>, rng: &mut R) -> Vec<RealUnit>
    where
        R: Rng + Sized,
    {
        let len = current_gen.size();
        let mut fit_sum = 0.0;
        let mut max_value = 0.0;
        for unit in current_gen.units().iter() {
            let fit = unit.get_fitness_scalar();
            fit_sum += fit;
            if fit > max_value {
                max_value = fit;
            }
        }

        fit_sum = (len as f64) * max_value - fit_sum;
        let rnum = rng.gen_range(0.0, 1.0) * fit_sum;
        let mut accumlated = 0.0;

        for (i, unit) in current_gen.units().iter().enumerate() {
            let fit = unit.get_fitness_scalar();
            accumlated += max_value - fit;
            if rnum < accumlated {
                return vec![current_gen.units().get(i).unwrap().clone()];
            }
        }
        return vec![current_gen.units().get(len - 1).unwrap().clone()];
    }
}

pub struct TourneySelect {
    k: usize,
}

impl TourneySelect {
    pub fn new(k: usize) -> TourneySelect {
        TourneySelect { k: k }
    }
}

impl SelectionOp<RealUnit, f64> for TourneySelect {
    fn select_from<R>(&self, current_gen: &Population<RealUnit>, rng: &mut R) -> Vec<RealUnit>
    where
        R: Rng + Sized,
    {
        let mut candidates = Vec::new();
        for _ in 0..self.k {
            let index = rng.gen_range(0, current_gen.size());
            candidates.push(current_gen.units().get(index).unwrap().clone());
        }
        candidates
    }
}

pub struct BlxAlpha {
    pub alpha: f64,
}

impl CrossoverOp<RealUnit> for BlxAlpha {
    fn crossover<R>(&self, parents: Vec<RealUnit>, rng: &mut R) -> Vec<RealUnit>
    where
        R: Rng + Sized,
    {
        let fst = parents.get(0).unwrap().get_chromo();
        let snd = parents.get(1).unwrap().get_chromo();
        let len = fst.len();
        let mut new_chromosomes = vec![0.0; len];

        for i in 0..len {
            let c_min = fst[i].min(snd[i]);
            let c_max = fst[i].max(snd[i]);
            let interval = c_max - c_min;
            new_chromosomes[i] = ((c_max + interval * self.alpha)
                - (c_min - interval * self.alpha))
                * rng.gen_range(0.0, 1.0)
                + (c_min - interval * self.alpha);
        }
        vec![RealUnit::from_vec(new_chromosomes)]
    }
}

pub struct RealMutator {
    /// Probability of mutation
    pub mutation_probability: f64,
    /// Strength of mutation
    pub mutation_strength: f64,
}

impl MutationOp<RealUnit> for RealMutator {
    fn mutate<R>(&self, unit: RealUnit, rng: &mut R) -> RealUnit
    where
        R: Rng + Sized,
    {
        let len = unit.get_chromo().len();
        let mut new = unit.get_chromo().clone();

        for i in 0..len {
            if rng.gen_range(0.0, 1.0) < self.mutation_probability {
                let rnum: f64 = rng.sample(StandardNormal);
                new[i] += rnum * self.mutation_strength;
            }
        }
        RealUnit::from_vec(new)
    }
}

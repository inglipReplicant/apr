/// Real valued chromosomes and corresponding mechanisms
use crate::{
    apr_functions::{Applicable, GeneticCaller},
    fitness::{Fitness, FitnessFunction},
    ops::*,
    population::Population,
    unit::Unit,
};

use rand::{thread_rng, Rng};
use rand_distr::StandardNormal;

#[derive(Clone, Debug, PartialEq)]
pub struct BoundedUnit {
    chromosome: Vec<f64>,

    fitness: f64,
}

#[derive(Clone)]
pub struct Bound(f64, f64);

impl Bound {
    pub fn new(lower: f64, upper: f64) -> Bound {
        Bound(lower, upper)
    }
}

impl BoundedUnit {
    pub fn new_with_bounds(chromosome: Vec<f64>, fitness: f64, bound: &Bound) -> BoundedUnit {
        let mut c = chromosome;
        for i in 0..c.len() {
            if c[i] > bound.1 {
                c[i] = bound.1;
            }
            if c[i] < bound.0 {
                c[i] = bound.0;
            }
        }
        BoundedUnit::new(c, fitness)
    }

    pub fn new(chromosome: Vec<f64>, fitness: f64) -> BoundedUnit {
        BoundedUnit {
            chromosome: chromosome,
            fitness: fitness,
        }
    }

    pub fn from_vec(chromosome: Vec<f64>) -> BoundedUnit {
        BoundedUnit {
            chromosome: chromosome,
            fitness: 0.0,
        }
    }
}

impl Unit for BoundedUnit {
    type Chromosome = Vec<f64>;
    type Fitness = f64;

    fn get_chromo(&self) -> &Vec<f64> {
        &self.chromosome
    }

    fn get_fitness(&self) -> &f64 {
        &self.fitness
    }

    fn update_fitness(&mut self, fit: f64) {
        self.fitness = fit;
    }

    fn get_fitness_scalar(&self) -> f64 {
        self.fitness.to_scalar()
    }

    fn update_chromosome(&mut self, chromosome: Vec<f64>) {
        self.chromosome = chromosome;
    }
}

impl Applicable for BoundedUnit {
    fn as_floats(&self) -> &Vec<f64> {
        &self.chromosome
    }
}

impl FitnessFunction<BoundedUnit, f64> for GeneticCaller<BoundedUnit> {
    fn fitness_of(&mut self, x: &BoundedUnit) -> f64 {
        self.apply(x)
    }

    fn fitness_min(&self) -> f64 {
        0.0
    }

    fn fitness_max(&self) -> f64 {
        std::f64::MAX
    }
}

pub struct RouletteSelect {}

impl SelectionOp<BoundedUnit, f64> for RouletteSelect {
    fn select_from<R>(&self, current_gen: &Population<BoundedUnit>, rng: &mut R) -> Vec<BoundedUnit>
    where
        R: Rng + Sized,
    {
        let len = current_gen.size();
        let mut fit_sum = 0.0;
        let mut max_value = 0.0;
        for unit in current_gen.units().iter() {
            let fit = unit.get_fitness_scalar();
            fit_sum += fit;
            if fit > max_value {
                max_value = fit;
            }
        }

        fit_sum = (len as f64) * max_value - fit_sum;
        let rnum = rng.gen_range(0.0, 1.0) * fit_sum;
        let mut accumlated = 0.0;

        for (i, unit) in current_gen.units().iter().enumerate() {
            let fit = unit.get_fitness_scalar();
            accumlated += max_value - fit;
            if rnum < accumlated {
                return vec![current_gen.units().get(i).unwrap().clone()];
            }
        }
        return vec![current_gen.units().get(len - 1).unwrap().clone()];
    }
}

pub struct TourneySelect {
    k: usize,
}

impl TourneySelect {
    pub fn new(k: usize) -> TourneySelect {
        TourneySelect { k: k }
    }
}

impl SelectionOp<BoundedUnit, f64> for TourneySelect {
    fn select_from<R>(&self, current_gen: &Population<BoundedUnit>, rng: &mut R) -> Vec<BoundedUnit>
    where
        R: Rng + Sized,
    {
        let mut candidates = Vec::new();
        for _ in 0..self.k {
            let index = rng.gen_range(0, current_gen.size());
            candidates.push(current_gen.units().get(index).unwrap().clone());
        }
        candidates
    }
}

pub struct BlxAlpha {
    pub alpha: f64,
}

impl CrossoverOp<BoundedUnit> for BlxAlpha {
    fn crossover<R>(&self, parents: Vec<BoundedUnit>, rng: &mut R) -> Vec<BoundedUnit>
    where
        R: Rng + Sized,
    {
        let fst = parents.get(0).unwrap().get_chromo();
        let snd = parents.get(1).unwrap().get_chromo();
        let len = fst.len();
        let mut new_chromosomes = vec![0.0; len];

        for i in 0..len {
            let c_min = fst[i].min(snd[i]);
            let c_max = fst[i].max(snd[i]);
            let interval = c_max - c_min;
            new_chromosomes[i] = ((c_max + interval * self.alpha)
                - (c_min - interval * self.alpha))
                * rng.gen_range(0.0, 1.0)
                + (c_min - interval * self.alpha);
        }
        vec![BoundedUnit::from_vec(new_chromosomes)]
    }
}

pub struct RealMutator {
    /// Probability of mutation
    pub mutation_probability: f64,
    /// Strength of mutation
    pub mutation_strength: f64,
}

impl MutationOp<BoundedUnit> for RealMutator {
    fn mutate<R>(&self, unit: BoundedUnit, rng: &mut R) -> BoundedUnit
    where
        R: Rng + Sized,
    {
        let len = unit.get_chromo().len();
        let mut new = unit.get_chromo().clone();

        for i in 0..len {
            if rng.gen_range(0.0, 1.0) < self.mutation_probability {
                let rnum: f64 = rng.sample(StandardNormal);
                new[i] += rnum * self.mutation_strength;
            }
        }
        BoundedUnit::from_vec(new)
    }
}

pub fn generate_initial_genetic(
    size: usize,
    solution_len: usize,
    bound: &Bound,
    evaluator: &mut GeneticCaller<BoundedUnit>,
) -> Population<BoundedUnit> {
    let mut units = Vec::new();
    let mut rng = thread_rng();
    for _ in 0..size {
        let mut chromos = Vec::new();
        for _ in 0..solution_len {
            chromos.push(rng.gen_range(bound.0, bound.1));
        }
        let mut unit = BoundedUnit::from_vec(chromos);
        let fit = evaluator.fitness_of(&unit);
        unit.update_fitness(fit);
        units.push(unit);
    }
    Population::from_units(units)
}

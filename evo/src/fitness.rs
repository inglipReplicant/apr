use crate::unit::Unit;
use std::fmt::Debug;

pub trait Fitness: PartialEq + PartialOrd + Clone + Debug + Sized {
    fn zero() -> Self;

    fn abs_diff(&self, other: &Self) -> Self;

    fn to_scalar(&self) -> f64;
}

pub trait FitnessFunction<G, F>: Clone
where
    G: Unit,
    F: Fitness,
{
    fn fitness_of(&mut self, x: &G) -> F;

    //fn average(&self, a: &[F]) -> F;

    fn fitness_max(&self) -> F;

    fn fitness_min(&self) -> F;
}

impl Fitness for f64 {
    fn zero() -> f64 {
        0.0
    }

    fn abs_diff(&self, other: &f64) -> f64 {
        (self - other).abs()
    }

    fn to_scalar(&self) -> f64 {
        *self
    }
}

pub mod algorithm;
pub mod apr_binary;
pub mod apr_functions;
pub mod apr_real;
pub mod fitness; //Fitness, Fitness Function
pub mod genetic;
pub mod ops;
pub mod population; //Population
pub mod real;
pub mod unit; //Unit, chromosome
